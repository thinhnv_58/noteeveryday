package com.example.thinhnv.noteeveryday.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.adapter.NoteListAdapter;
import com.example.thinhnv.noteeveryday.model.NoteContract;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;

    private NoteDBHelper dbHelper=new NoteDBHelper(this);
    private NoteListAdapter listNoteAdapter;
    private ListView listViewNoteToDay;
    private Button btAddNote;
    Context mContext;

    public static final String MESSAGE="MESSAGE_FROM_ADDNOTEACTIVITY";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=(Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //set up context is this
        mContext=this;



        // find view by id button and ListView
        btAddNote=(Button) findViewById(R.id.btAddNote);
        listViewNoteToDay=(ListView) findViewById(R.id.listViewNoteToDay);
        // set adapter
        listNoteAdapter=new NoteListAdapter(this, R.layout.item_note, dbHelper.getNotes());
        listViewNoteToDay.setAdapter(listNoteAdapter);

        // set button click listener
        btAddNote.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i=new Intent(mContext, AddNoteActivity.class);
               startActivityForResult(i, 2);
           }
        });

        // set item long click listener cai dat hop thong bao xoa note
        listViewNoteToDay.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final long getId=id;
                final AlertDialog.Builder builder=new AlertDialog.Builder(mContext);
                builder.setMessage("Are you delete '"+dbHelper.getNote(id).getDetail()+"' ?" );
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dbHelper.deleteNote(getId);
                        listNoteAdapter.notifyDataChange();
                        listNoteAdapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        builder.setCancelable(true);
                    }
                });
                builder.create().show();
                return  false;


            }
        });

    }


    // activity for result to get result of note addnotactiivty
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2){
           if (resultCode==2){
               String text=data.getStringExtra(MESSAGE);
               dbHelper.createNote(new NoteModel(text));
               listNoteAdapter.notifyDataChange();
               listNoteAdapter.notifyDataSetChanged();
           }
        }
    }






















    // cai nay khong quan trong
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
