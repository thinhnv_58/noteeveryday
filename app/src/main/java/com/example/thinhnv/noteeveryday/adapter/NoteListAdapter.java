package com.example.thinhnv.noteeveryday.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thinhnv.noteeveryday.R;
import com.example.thinhnv.noteeveryday.model.NoteDBHelper;
import com.example.thinhnv.noteeveryday.model.NoteModel;

import java.util.List;

/**
 * Created by thinhnv on 08/10/2015.
 */
public class NoteListAdapter extends BaseAdapter {
    private Context context;
    private List<NoteModel> listNote;
    NoteDBHelper dbHelper;

    public NoteListAdapter(Context context, int item_note, List<NoteModel> listNote){
        this.context=context;
        this.listNote=listNote;
        dbHelper=new NoteDBHelper(context);
    }

    @Override
    public int getCount() {
        return listNote.size();
    }

    @Override
    public Object getItem(int position) {
        return listNote.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listNote.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.item_note, parent, false);
        }
        NoteModel model=listNote.get(position);
        TextView tvItemDetail= (TextView) convertView.findViewById(R.id.tvItemDetail);
        tvItemDetail.setText(position+1+" "+model.getDetail());
        return convertView;
    }

    public void notifyDataChange(){
        listNote=dbHelper.getNotes();

    }


}
