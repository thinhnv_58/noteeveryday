package com.example.thinhnv.noteeveryday.model;

/**
 * Created by thinhnv on 08/10/2015.
 */
public class NoteModel {
    public long id;
    private String mDetail;

    public NoteModel(){}

    public NoteModel(String detail){
        this.mDetail=detail;
    }

    public String getDetail(){
        return this.mDetail;
    }
    public void setDetail(String detail){
        this.mDetail=detail;
    }
}
