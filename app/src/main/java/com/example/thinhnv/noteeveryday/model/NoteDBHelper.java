package com.example.thinhnv.noteeveryday.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thinhnv on 08/10/2015.
 */
public class NoteDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="namedatabase.db";
    private static final String SQL_CREATE_NOTE="CREATE TABLE "+
            NoteContract.TABLE_NAME+" ( "+
            NoteContract._ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            NoteContract.DETAILS+" TEXT)";
    private static final String SQL_DELETE_NOTE="DROP TABLE IF EXISTS "+NoteContract.TABLE_NAME;

    public NoteDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_NOTE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_NOTE);
        onCreate(db);
    }

    private NoteModel populateModel(Cursor c){
        NoteModel model=new NoteModel();
        model.id=c.getLong(c.getColumnIndex(NoteContract._ID));
        model.setDetail(c.getString(c.getColumnIndex(NoteContract.DETAILS)));
        return model;
    }

    private ContentValues populateContent(NoteModel model){
        ContentValues values=new ContentValues();
        values.put(NoteContract.DETAILS, model.getDetail());
        return values;
    }


    public long createNote(NoteModel model){
        ContentValues values=populateContent(model);
        return getWritableDatabase().insert(NoteContract.TABLE_NAME, null, values);
    }

    public NoteModel getNote(long id){
        SQLiteDatabase db=this.getReadableDatabase();
        String select="SELECT * FROM "+NoteContract.TABLE_NAME+" WHERE "+NoteContract._ID+ " = "+id;
        Cursor c=db.rawQuery(select, null);
        if (c.moveToNext()){
            return populateModel(c);
        }
        return null;
    }


    public List<NoteModel> getNotes(){
        SQLiteDatabase db=this.getReadableDatabase();
        String select="SELECT * FROM "+NoteContract.TABLE_NAME;
        Cursor c=db.rawQuery(select, null);
        List<NoteModel> listNote=new ArrayList<NoteModel>();

        while(c.moveToNext()){
            listNote.add(populateModel(c));

        }

        if (!listNote.isEmpty()){
            return listNote;
        }

        return null;
    }

    public int deleteNote(long id){
        return getWritableDatabase().delete(NoteContract.TABLE_NAME, NoteContract._ID+"=?", new String[]{String.valueOf(id)});
    }


    public long updateNote(NoteModel model){
        ContentValues values=populateContent(model);
        return getWritableDatabase().update(NoteContract.TABLE_NAME, values, NoteContract._ID +"=?", new String[]{String.valueOf(model.id)});
    }



}
